# react-protected-route

> Simple react-router Route wrapper to secure protected/authenticated Route/Page

[![NPM](https://img.shields.io/npm/v/react-protected-route.svg)](https://www.npmjs.com/package/react-protected-route) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save react-protected-route
```

## Usage (single route)

```jsx
import React from 'react'

import { ProtectedRoute } from '@raeadvent/react-protected-route'
import Dashboard from 'pages/Dashboard'

const RouteCollection = () => {

  const urlCondition = true // Boolean value if false fallback to redirect url you can add as many condition as you want as long as you return a boolean value

  return (
  <ProtectedRoute
    path="/admin"
    condition={urlCondition}
    redirectUrl="/login"
    component={<Dashboard />}
  />
  )
}
...
```

## Usage (multiple routes)

```jsx
import React from 'react'

import { Route, Redirect } from 'react-router-dom'
import { ProtectedChildren } from '@raeadvent/react-protected-route'

import Dashboard from 'pages/Dashboard'
import SecuredPage from 'pages/SecuredPage'

const RouteCollection = () => {
  const urlCondition = true // Boolean value if false fallback to redirect url you can add as many condition as you want as long as you return a boolean value

  return (
    <ProtectedChildren
      path="/admin"
      condition={urlCondition}
      redirectUrl="/login"
    >
      <Route exact path="/Dashboard" component={Dashboard} />
      <Route path="/SecuredPage" component={SecuredPage} />
    </ProtectedChildren>
  )
}
```

## License

MIT © [Raeneldis A. Sadra]

```

```
