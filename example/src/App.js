import React from 'react'

import { ExampleComponent } from 'react-protected-route'
import 'react-protected-route/dist/index.css'

const App = () => {
  return <ExampleComponent text="Create React Library Example 😄" />
}

export default App
