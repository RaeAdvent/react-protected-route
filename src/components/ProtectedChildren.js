import React, { Fragment } from 'react'
import { Route, Redirect } from 'react-router-dom'

export const ProtectedChildren = ({
  condition,
  path,
  redirectUrl,
  children,
  ...props
}) => {
  return !condition ? (
    <Route
      render={() => (
        <Redirect
          to={{
            pathname: `${redirectUrl}`,
          }}
        />
      )}
    />
  ) : (
    <Fragment>{children}</Fragment>
  )
}
