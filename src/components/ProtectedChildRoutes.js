import React from 'react'
import { Route, Redirect, Switch } from 'react-router-dom'

export const ProtectedChildRoutes = ({
  condition,
  children,
  redirectUrl,
  components,
  ...props
}) => {
  return (
    <Switch>
      {components.map((c) => {
        return (
          <Route
            key={c.path}
            exact
            path={c.path}
            render={() =>
              !condition ? (
                <Redirect
                  to={{
                    pathname: `${redirectUrl}`,
                  }}
                />
              ) : (
                c.component
              )
            }
          />
        )
      })}
      {children}
    </Switch>
  )
}
