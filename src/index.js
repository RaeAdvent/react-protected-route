import React, { Fragment } from 'react'
import { Route, Redirect } from 'react-router-dom'
import { ProtectedChildren } from './components/ProtectedChildren'
import { ProtectedChildRoutes } from './components/ProtectedChildRoutes'

const ProtectedRoute = ({
  condition,
  path,
  component,
  redirectUrl,
  ...props
}) => {
  return (
    <Route
      path={path}
      render={() =>
        !condition ? (
          <Redirect
            to={{
              pathname: `${redirectUrl}`,
            }}
          />
        ) : (
          component
        )
      }
    />
  )
}

export { ProtectedRoute, ProtectedChildren, ProtectedChildRoutes }
